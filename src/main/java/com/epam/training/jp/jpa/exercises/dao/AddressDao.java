package com.epam.training.jp.jpa.exercises.dao;

import com.epam.training.jp.jpa.exercises.domain.Address;


public interface AddressDao {

	void save(Address address);
}
