package com.epam.training.jp.jpa.exercises.dao;

import com.epam.training.jp.jpa.exercises.domain.FoodOrder;

public interface FoodOrderDao {

	void save(FoodOrder foodOrder);
	
	
}
