package com.epam.training.jp.jpa.exercises.dao;

import java.util.List;

import com.epam.training.jp.jpa.exercises.domain.Food;
import com.epam.training.jp.jpa.exercises.domain.Menu;

public interface MenuDao {

	public List<Menu> getActualMenus();

	public Food findFoodById(int foodId);
}
