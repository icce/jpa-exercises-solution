package com.epam.training.jp.jpa.exercises.dao;

import com.epam.training.jp.jpa.exercises.domain.OrderItem;

public interface OrderItemDao {

	void save(OrderItem orderItem);
	
}
