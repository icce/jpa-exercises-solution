package com.epam.training.jp.jpa.exercises.dao.jpaimpl;

import com.epam.training.jp.jpa.exercises.dao.AddressDao;
import com.epam.training.jp.jpa.exercises.domain.Address;

public class JpaAddressDao extends GenericJpaDao implements AddressDao {

	@Override
	public void save(Address address) {
		entityManager.persist(address);

	}
}
