package com.epam.training.jp.jpa.exercises.dao.jpaimpl;

import com.epam.training.jp.jpa.exercises.dao.FoodOrderDao;
import com.epam.training.jp.jpa.exercises.domain.FoodOrder;

public class JpaFoodOrderDao extends GenericJpaDao implements FoodOrderDao {

	@Override
	public void save(FoodOrder foodOrder) {
		entityManager.persist(foodOrder);
	}

}
