package com.epam.training.jp.jpa.exercises.dao.jpaimpl;

import java.util.Date;
import java.util.List;

import com.epam.training.jp.jpa.exercises.dao.MenuDao;
import com.epam.training.jp.jpa.exercises.domain.Food;
import com.epam.training.jp.jpa.exercises.domain.Menu;

public class JpaMenuDao extends GenericJpaDao implements MenuDao {

	@Override
	public List<Menu> getActualMenus() {
		
		//Solution 1: JPQL
		List<Menu> menus = entityManager
				.createQuery("select m from Menu m where :now >= m.fromDate and :now <= m.toDate", Menu.class)
				.setParameter("now", new Date())
				.getResultList();
		for (Menu menu : menus) {
			menu.getFoods().size();
		}
		return menus;
		
		//Solution 2: JPQL with fetch join
//		List<Menu> menus = entityManager
//				.createQuery("select m from Menu m join fetch m.foods where :now >= m.fromDate and :now <= m.toDate", Menu.class)
//				.setParameter("now", new Date())
//				.getResultList();
//		return menus;
		
		//Solution 3: Criteria API
//		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//		CriteriaQuery<Menu> c = cb.createQuery(Menu.class);
//		Root<Menu> root = c.from(Menu.class);
//		root.fetch("foods"); 
//		ParameterExpression<Date> today = cb.parameter(Date.class);
//	
//		CriteriaQuery<Menu> criteriaQuery = c.select(root).where(cb.between(
//			today, root.<Date>get("fromDate"), root.<Date>get("toDate")));
//	  
//		TypedQuery<Menu> query = entityManager.createQuery(criteriaQuery);
//		
//		query.setParameter(today, new Date());
//		return query.getResultList();
		
	}

	@Override
	public Food findFoodById(int foodId) {
		return entityManager.find(Food.class, foodId);
	}

}
