package com.epam.training.jp.jpa.exercises.dao.jpaimpl;

import com.epam.training.jp.jpa.exercises.dao.OrderItemDao;
import com.epam.training.jp.jpa.exercises.domain.OrderItem;

public class JpaOrderItemDao extends GenericJpaDao implements OrderItemDao {

	@Override
	public void save(OrderItem orderItem) {
		entityManager.persist(orderItem);

	}

}
