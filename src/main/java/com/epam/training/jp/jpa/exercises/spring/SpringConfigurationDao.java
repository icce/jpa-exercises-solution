package com.epam.training.jp.jpa.exercises.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;

import com.epam.training.jp.jpa.exercises.dao.AddressDao;
import com.epam.training.jp.jpa.exercises.dao.FoodOrderDao;
import com.epam.training.jp.jpa.exercises.dao.MenuDao;
import com.epam.training.jp.jpa.exercises.dao.OrderItemDao;
import com.epam.training.jp.jpa.exercises.dao.jpaimpl.JpaAddressDao;
import com.epam.training.jp.jpa.exercises.dao.jpaimpl.JpaFoodOrderDao;
import com.epam.training.jp.jpa.exercises.dao.jpaimpl.JpaMenuDao;
import com.epam.training.jp.jpa.exercises.dao.jpaimpl.JpaOrderItemDao;

public class SpringConfigurationDao {

	@Bean
	public AddressDao addressDao() {
		return new JpaAddressDao();
	}
	
	@Bean
	public FoodOrderDao foodOrderDao() {
		return new JpaFoodOrderDao();
	}
	
	@Bean
	public MenuDao menuDao() {
		return new JpaMenuDao();
	}
	
	@Bean
	public OrderItemDao orderItemDao() {
		return new JpaOrderItemDao();
	}
	
	@Bean
	public PersistenceExceptionTranslationPostProcessor petpp() {
		return new PersistenceExceptionTranslationPostProcessor();
	}
	
}

