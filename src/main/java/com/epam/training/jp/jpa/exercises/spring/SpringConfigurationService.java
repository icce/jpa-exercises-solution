package com.epam.training.jp.jpa.exercises.spring;

import org.springframework.context.annotation.Bean;

import com.epam.training.jp.jpa.exercises.dao.AddressDao;
import com.epam.training.jp.jpa.exercises.dao.FoodOrderDao;
import com.epam.training.jp.jpa.exercises.dao.MenuDao;
import com.epam.training.jp.jpa.exercises.dao.OrderItemDao;
import com.epam.training.jp.jpa.exercises.dto.ShoppingCartFactory;
import com.epam.training.jp.jpa.exercises.service.OrderService;
import com.epam.training.jp.jpa.exercises.service.PopulateDatabaseService;

public class SpringConfigurationService {

	@Bean
	public OrderService orderService(AddressDao addressDao, FoodOrderDao foodOrderDao, MenuDao menuDao, OrderItemDao orderItemDao) {
		return new OrderService(menuDao, foodOrderDao, addressDao, orderItemDao);
	}
	
	@Bean
	public ShoppingCartFactory shoppingCartFactory(OrderService orderService) {
		return new ShoppingCartFactory(orderService);
	}
	
	@Bean(initMethod = "popluateDatabase")
	public PopulateDatabaseService populateDatabaseService() {
		return new PopulateDatabaseService();
	}
}
